import argparse
import json
import cv2
import time
from datetime import datetime
from datetime import timedelta

from . import predict, client, imgprovider


class App:
    def creatediff(self, states):
        result = {}
        for id, _ in states.items():
            if id not in self.prevstates or self.prevstates[id] != states[id]:
                result[id] = states[id]

        return result

    def updatediff(self, states):
        for id, _ in states.items():
            self.prevstates[id] = states[id]


    def run(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("--config", "-c", help="config file location", default="config.json")

        args = parser.parse_args()

        print(f"Starting up {datetime.now()}")
        self.prevstates = {}

        # Load configuration
        self.cfg = {}
        with open(args.config, "r") as f:
            self.cfg = json.load(f)

        print(self.cfg)

        # Load pointdef
        self.pointdef = []
        with open(self.cfg["pointdef"], "r") as f:
            self.pointdef = json.load(f)

        # Setup needed subsystems
        self.pred = predict.Predictor(self.cfg, self.pointdef)
        self.imgprov = imgprovider.Imgprov(self.cfg)
        self.reporter = client.Reporter(self.cfg, self.pointdef)

        # Startup main loop
        self.updateloop()


    def sendupdate(self):
        # Get at new image
        img, measured = self.imgprov.getimage()
        # Run it through the ai
        states = self.pred.predict(img)

        # Calculate diff
        states = self.creatediff(states)
        if len(states) == 0:
            return

        # Send it to upstream
        if self.reporter.send(states, measured):
            self.updatediff(states)


    def updateloop(self):
        next_call = datetime.now()
        while True:
            print(f"Sending update at { datetime.now() }")
            self.sendupdate()

            # Wait for next event
            next_call += timedelta(seconds=self.cfg["updatedelay"])
            waittime = next_call - datetime.now()
            waittime = waittime.total_seconds()
            if waittime < 0:
                print(f"Missed deadline with {-waittime} seconds")
                continue
            print(f"Sleeping for {waittime}")
            time.sleep(waittime)


