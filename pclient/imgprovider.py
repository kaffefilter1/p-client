
import importlib.util as importutil

class Imgprov:
    def __init__(self, cfg):

        cfg = cfg["imgprov"]

        # Load image module
        spec = importutil.spec_from_file_location("module.name", cfg["modpath"])
        mod = importutil.module_from_spec(spec)
        spec.loader.exec_module(mod)

        # Create a instance of the class from the module
        self.img = mod.ImgCapture(cfg["modattr"])


    def getimage(self):
        # Run the capture function and return the result
        img, timestamp = self.img.capture()

        return img, timestamp

