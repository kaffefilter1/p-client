
import requests
import os.path as path
import json
import sys
import random

class Reporter:
    def __init__(self, cfg, pointdef):
        if "statefile" in cfg:
            self.statefile = cfg["statefile"]
        else:
            self.statefile = "state.json"
        self.cfg = cfg
        self.pointdef = pointdef

        # Check if statefile exists
        if not path.isfile(self.statefile):
            print("Registering on server")
            # Register on upstream
            self.register()
        else:
            print("Loading statefile statefile")
            with open(self.statefile, "r") as f:
                self.upstreamids = json.load(f)


    def register(self):
        data = []
        # Extract data from points
        for spot in self.pointdef:
            spotdat = {
                    "location": spot.get("location", (random.uniform(0, 9.9), random.uniform(0, 9.9))),
                    "floor": spot.get("floor", 0),
                    "flags": spot.get("flags", [])
            }
            data += [ spotdat ]

        # Decode as json
        data = json.dumps(data)

        area = self.cfg["areaid"]
        # Send
        r = requests.post(path.join(self.cfg["upstream"], f"area/{area}/register"),
                         data = data,
                         verify=False)
        if r.status_code != 201:
            print(f"could not register on upstream, response {r.status_code}:", file=sys.stderr)
            print(f"server responded with: {r.json()['error']}", file=sys.stderr)
            sys.exit(1)

        r = r.json()
        self.upstreamids = {}
        for i, spot in enumerate(self.pointdef):
            self.upstreamids[spot["id"]] = r[i]

        # Save the allocated ids to statefile
        with open(self.statefile, "w") as f:
            json.dump(self.upstreamids, f)

    def send(self, states, measured):
        spotstates = []
        for (id, occupied) in states.items():
            state = {
                    "id": self.upstreamids[id],
                    "av": not occupied
            }
            spotstates += [state]

        report = json.dumps({
                "ms": measured.strftime("%Y-%m-%dT%H:%M:%S"),
                "ss": spotstates
        })

        area = self.cfg["areaid"]
        r = requests.post(path.join(self.cfg["upstream"], f"area/{area}/report"),
                data = report,
                verify = False)
        if r.status_code != 200:
            print(f"Report measued {measured} failed with code {r.status_code}", file=sys.stderr)
            print(f"server responded with: {r.json()['error']}", file=sys.stderr)
            return False

        return True


