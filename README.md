# p-client

Sends reports to p-upstream.

## Running

Start by training a network using vision1exp. 
Save the `export.pkl` in a folder called `learn`.
Also copy the pointdefinition to `points.json`.

Point the `upstream` to your running p-upstream instance in `config.json`.

Then start with:

```bash
python -m pclient
```

