
import cv2
import os
from datetime import datetime
import os.path as path

class ImgCapture:

    def __init__(self, attr):
        self.foldername = attr["folder"]

        # List imagefiles in folder
        self.files = os.listdir(self.foldername)
        self.files.sort()

        self.index = 0

    def capture(self):
        if self.index >= len(self.files):
            raise Exception("could not pull image: out of images")

        # Piece together path to imagefile
        fname = path.join(self.foldername, self.files[self.index])

        # Load image
        img = cv2.imread(fname)

        self.index += 1

        return img, datetime.now()


    def close():
        pass
