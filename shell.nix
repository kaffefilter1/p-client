let
  pkgs = import <nixpkgs> {};
  stdenv = pkgs.stdenv;

in stdenv.mkDerivation {
  name = "visionp4-env";

  buildInputs = with pkgs; [
    opencv4
    python37
    python37Packages.virtualenv
    python37Packages.pygobject3
    python37Packages.opencv4
  ];
}
